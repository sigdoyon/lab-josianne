exports.getMimeType = function(filename) {		
	let mimeType = 'text/html';
	if (filename.indexOf('.css') !== -1) {
		mimeType = 'text/css';
	}
	if (filename.indexOf('.png') !== -1) {
		mimeType = 'image/png';
	}

	return mimeType;
}
