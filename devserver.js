var http = require('http');
var fs = require('fs');
var url = require('url');
var MimeType = require('./MimeType');

http.createServer(function(request, response) {
    var query = url.parse(request.url, true);  
    console.log('query.pathname: ', query.pathname);	
	if (query.pathname === '/') {
		query.pathname = 'index.html';
	}
	var filename = "./sites/" + query.pathname;	
    fs.readFile(filename, function (err, html) {
		let mimeType = MimeType.getMimeType(filename);
        if (err) {
            response.writeHead(404, {'Content-Type': mimeType});
            return response.end("404 Not Found");
        }
	
        response.writeHeader(200, {'Content-Type': mimeType});
        response.write(html);
        response.end();
    });
}).listen(8000);
