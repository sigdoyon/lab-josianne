# README #

* NodeWebServer is a basic Web server using Node JS.
* Version: 1.0.0
* [Learn Markdown](https://sigdoyon.ca)

### How do I get set up? ###

Copy NodeWebServer folder into your working directory. 
Node JS is required in order to run the server. Node JS version 12.8.2 is recommended.
By default, the server is listening to port 8000. To test the server, launch a web browser and type the URL : http://localhost:8000. 
The test page index.html should display.
Please note https protocol is not yet supported.

### Contribution guidelines ###

Yet to come.
* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Les Solutions Informatiques Gabriel Doyon
* Contact: sigdoyon@gmail.com.